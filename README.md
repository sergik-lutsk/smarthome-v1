# SmartHome_v1
This is my first training project in Visual Studio C#.NET

Application main tabs:
1. SmartHome device settings/control page - project on stm32 MCU development board. project link: http://
2. RGB-Tape device settings/control page - project on stm32f103 MCu dev. board, project link: http://
3. VLC player (Favorites, Main lists, controlled with IR-Remote connected to SmartHome device).
4. Audio tools page (under construction) - for calculate frequency and synchronize with RGB-Tape controller.
5. Log page
6. esp8266 (under construction) - project on esp8266 MCU connected to stm32 SmartHome devicevia UART. (WiFi GET/POST Web server for future controlling from Android devices)
7. VideoCAM (under construction) - Video functions...motion e.t.c

